package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"

	seg_byemptypara "solirom.ro/solirom/segmentation-service/byemptypara"
	seg_processcontent "solirom.ro/solirom/segmentation-service/processcontent"
)

func main() {
	port := ":7777"

	http.HandleFunc("/", segmentationHandler)

	log.Println("Listening on " + port + "...")
	err := http.ListenAndServe(port, nil)
	if err != nil {
		log.Fatal(err)
	}
}

func segmentationHandler(responseWriter http.ResponseWriter, request *http.Request) {
	tmpDir, err := ioutil.TempDir(os.TempDir(), "solirom-")
	check(err)
	htmlDir := filepath.Join(tmpDir, "html")
	os.Mkdir(htmlDir, os.ModePerm)
	jsonDir := filepath.Join(tmpDir, "json")
	os.Mkdir(jsonDir, os.ModePerm)
	//defer os.RemoveAll(tmpDir)
	fmt.Println(tmpDir)

	contentAsBytes, err := ioutil.ReadAll(request.Body)
	check(err)
	defer request.Body.Close()
	normalizedContent, err := seg_processcontent.ConvertToUTF8(contentAsBytes)
	check(err)

	parsedContent, resultMessages, err := seg_processcontent.ParseContent(normalizedContent, "p b span i sup")
	check(err)

	responseWriter.Write([]byte(resultMessages["fileConverted"]))

	segmentContentMessages, err := seg_byemptypara.SegmentContent(parsedContent, htmlDir)
	check(err)
	fmt.Println(segmentContentMessages)
	responseWriter.Write([]byte(segmentContentMessages["resultedEntries"]))

	responseWriter.Write([]byte(segmentContentMessages["headwordError"]))

	responseWriter.WriteHeader(200)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

// curl -X POST -H "Content-Type: application/octet-stream" --data-binary '@/home/claudius/workspace/repositories/sync/cflr-master/DELR/html/A C.htm' http://127.0.0.1:7777
// rsync -hsaPvzz --progress --stats -e "ssh -C -c aes128-ctr" claudius@85.186.121.41:/mnt/vol_back-up/clre/centos7.0.qcow2.tar.gz /home/claudius/workspace/repositories/xars/clre/
// curl -X POST -H "Content-Type: application/octet-stream" --data-binary '@/home/claudius/workspace/repositories/git/delr-data/2/xml/test.html' http://127.0.0.1:7777
// curl -X POST -H "Content-Type: application/octet-stream" --data-binary '@/home/claudius/workspace/repositories/git/delr-data/2/xml/test-error.html' http://127.0.0.1:7777
